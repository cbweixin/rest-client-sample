package com.weixin.rest.retrofit;

import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.weixin.rest.retrofit.model.IPMode;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class IPRequestDemo {
    private static final Logger LOGGER = Logger.getLogger(IPRequestDemo.class.getName());

    public void getSimple() {
        ConnectionPool pool = new ConnectionPool(5, 10000, TimeUnit.MILLISECONDS);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectionPool(pool)
                .build();
        // Get simple request
        final String url = "http://ip.taobao.com/service/";
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        IPService ipService = retrofit.create(IPService.class);
        Call<IPMode> ipMsg = ipService.getIpMsgGet();
        ipMsg.enqueue(new Callback<IPMode>() {
            @Override
            public void onResponse(Call<IPMode> call, Response<IPMode> response) {
                LOGGER.log(Level.SEVERE, response.code() + "");
                IPMode ipMode = response.body();
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getCode());
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getData().getCity());
            }

            @Override
            public void onFailure(Call<IPMode> call, Throwable t) {
                LOGGER.log(Level.SEVERE, "onFailure: ");
                t.printStackTrace();
            }
        });
    }

    private void getSimple2() {
        // Get simple request, using full url
        final String url = "http://ip.taobao.com/service/";
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        IPService ipService = retrofit.create(IPService.class);
        Call<IPMode> ipMsg = ipService.getIpMsgGet1("http://ip.taobao.com/service/getIpInfo.php?ip=118.114.168.175");
        ipMsg.enqueue(new Callback<IPMode>() {
            @Override
            public void onResponse(Call<IPMode> call, Response<IPMode> response) {
                LOGGER.log(Level.SEVERE, response.code() + "");
                IPMode ipMode = response.body();
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getCode());
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getData().toString());
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getData().getCity());
            }

            @Override
            public void onFailure(Call<IPMode> call, Throwable t) {
                LOGGER.log(Level.SEVERE, "onFailure: ");
                t.printStackTrace();
            }
        });
    }

    public void getParamesPath() {
        //        Get带参数请求(路径)
        final String baseUrl = "http://ip.taobao.com/";
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        IPService ipService = retrofit.create(IPService.class);
        Call<IPMode> ipMsg = ipService.getIpMsgGet("service");   //,"getIpInfo.php?ip=59.108.54.37"
        ipMsg.enqueue(new Callback<IPMode>() {
            @Override
            public void onResponse(Call<IPMode> call, Response<IPMode> response) {
                LOGGER.log(Level.SEVERE, response.code() + "");
                IPMode ipMode = response.body();
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getData().getCity());
            }

            @Override
            public void onFailure(Call<IPMode> call, Throwable t) {
                LOGGER.log(Level.SEVERE, "onFailure: ");
                t.printStackTrace();
            }
        });
    }

    public void getParamesIp() {
        //        Get带参数请求(IP)
        final String baseUrl = "http://ip.taobao.com/";
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        IPService ipService = retrofit.create(IPService.class);
        Call<IPMode> ipMsg = ipService.getIpMsgFromIpGet("60.108.54.37");   //,"getIpInfo.php?ip=59.108.54.37"
        ipMsg.enqueue(new Callback<IPMode>() {
            @Override
            public void onResponse(Call<IPMode> call, Response<IPMode> response) {
                LOGGER.log(Level.SEVERE, response.code() + "");
                IPMode ipMode = response.body();
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getCode());
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getData().getRegion());
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getData().getCity());
            }

            @Override
            public void onFailure(Call<IPMode> call, Throwable t) {
                LOGGER.log(Level.SEVERE, "onFailure: ");
                t.printStackTrace();
            }
        });
    }

    public void getParamesPathIp() {
        //        Get带参数请求(路径和IP)
        final String baseUrl = "http://ip.taobao.com/";
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        IPService ipService = retrofit.create(IPService.class);
        Call<IPMode> ipMsg = ipService.getIpMsgGet("service/getIpInfo.php", "62.108.54.37");   //,"getIpInfo.php?ip=59
        // .108.54.37"
        ipMsg.enqueue(new Callback<IPMode>() {
            @Override
            public void onResponse(Call<IPMode> call, Response<IPMode> response) {
                LOGGER.log(Level.SEVERE, response.code() + "");
                IPMode ipMode = response.body();
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getCode());
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getData().getRegion());
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getData().getCity());
            }

            @Override
            public void onFailure(Call<IPMode> call, Throwable t) {
                LOGGER.log(Level.SEVERE, "onFailure: ");
                t.printStackTrace();
            }
        });
    }

    public void getParamesQuaryMap() {
        //        Get带参数请求(动态指定查询条件组)
        final String baseUrl = "http://ip.taobao.com/";
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        IPService ipService = retrofit.create(IPService.class);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("ip", "59.108.54.37"); //Key 和 Value要对应，其中Key名要与网站对应
        Call<IPMode> ipMsg = ipService.getIpMsgGet(hashMap);
        ipMsg.enqueue(new Callback<IPMode>() {
            @Override
            public void onResponse(Call<IPMode> call, Response<IPMode> response) {
                LOGGER.log(Level.SEVERE, response.code() + "");
                LOGGER.log(Level.SEVERE, response.toString());
                IPMode ipMode = response.body();
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getCode());
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getData().getRegion());
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getData().getCity());
            }

            @Override
            public void onFailure(Call<IPMode> call, Throwable t) {
                LOGGER.log(Level.SEVERE, "onFailure: ");
                t.printStackTrace();
            }
        });
    }

    public void postParames() {
        //        Post带参数请求
        String url = "http://localhost:9999/";
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        IPService ipService = retrofit.create(IPService.class);
        //        Call<IpMode> ipMsg = ipService.getIpMsgPost("59.108.54.37");
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("ip", "59.108.54.37");
        Call<IPMode> ipMsg = ipService.getIpMsgPost(hashMap);
        ipMsg.enqueue(new Callback<IPMode>() {
            @Override
            public void onResponse(Call<IPMode> call, Response<IPMode> response) {
                LOGGER.log(Level.SEVERE, response.code() + "");
                LOGGER.log(Level.SEVERE, response.toString());
                IPMode ipMode = response.body();
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getCode());
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getData().getRegion());
                LOGGER.log(Level.INFO, "onResponse: " + ipMode.getData().getCity());
            }

            @Override
            public void onFailure(Call<IPMode> call, Throwable t) {
                LOGGER.log(Level.SEVERE, "onFailure: ");
                t.printStackTrace();
            }
        });
    }

    public static void main(String[] args) {
        IPRequestDemo demo = new IPRequestDemo();
        //        demo.getSimple();
        //        demo.getSimple2();
        //        demo.getParamesPath();
        //        demo.getParamesIp();
        //        demo.getParamesPathIp();
                demo.getParamesQuaryMap();
//        demo.postParames();
    }

}

