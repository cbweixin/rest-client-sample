package com.weixin.rest.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

import com.weixin.rest.retrofit.model.IPMode;

import java.util.Map;

public interface IPService {

    @Headers({
            "Accept-Encoding: application/json",
            "User-Agent: MoonRetrofit"  // this header is compulsory, otherwise exeption
    })
    @GET("getIpInfo.php?ip=59.108.54.37")
    Call<IPMode> getIpMsgGet();

    /**
     * use full url
     *
     * @param url
     * @return
     */
    @Headers({
            "Accept-Encoding: application/json",
            "User-Agent: MoonRetrofit"
    })
    @GET
    Call<IPMode> getIpMsgGet1(@Url String url);

    /**
     * 指定路径的访问
     *
     * @param pathStr 路径
     * @return
     */
    @Headers({
            "Accept-Encoding: application/json",
            "User-Agent: MoonRetrofit"
    })
    @GET("{path}/getIpInfo.php?ip=59.108.54.37")
    //{path}与@Path("path")参数对应，接受参数后{path}被替换
    Call<IPMode> getIpMsgGet(@Path("path") String pathStr);

    /**
     * 指定IP访问
     * 会组成全路径：http://ip.taobao.com/service/getIpInfo.php?ip=118.114.168.175
     * 在路径后自动添加  ?ip=ipStr
     *
     * @param ipStr
     * @return
     */
    @Headers({
            "Accept-Encoding: application/json",
            "User-Agent: MoonRetrofit"
    })
    @GET("service/getIpInfo.php")
    Call<IPMode> getIpMsgFromIpGet(@Query("ip") String ipStr);

    @Headers({
            "Accept-Encoding: application/json",
            "User-Agent: MoonRetrofit"
    })
    @GET("{path}")
    Call<IPMode> getIpMsgGet(@Path("path") String pathStr, @Query("ip") String ipStr);

    /**
     * 动态指定查询条件组
     *
     * @param quaryMap
     * @return
     */
    @Headers({
            "Accept-Encoding: application/json",
            "User-Agent: MoonRetrofit"
    })
    @GET("service/getIpInfo.php")
    Call<IPMode> getIpMsgGet(@QueryMap Map<String, String> quaryMap);

    /**
     * 带参数的POST请求
     * 用@Field("ip")标注请求的参数键值
     *
     * @param ip
     * @return
     */
    @Headers({
            "Accept-Encoding: application/json", //
            "User-Agent: MoonRetrofit"
    })
    @FormUrlEncoded  //注明是一个表单请求
    @POST("service/getIpInfo.php")
    Call<IPMode> getIpMsgPost(@Field("ip") String ip);

    @Headers({
            "Accept-Encoding: application/json", //
            "User-Agent: MoonRetrofit"
    })
    @FormUrlEncoded  //注明是一个表单请求
    @POST("service/getIpInfo.php")
    Call<IPMode> getIpMsgPost(@FieldMap Map<String, String> quaryMap);
}
